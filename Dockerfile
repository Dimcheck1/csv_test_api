FROM python:3.11-slim
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt update && apt install -y lsof

WORKDIR /csv-service
COPY requirements.txt /csv-service
RUN pip install -r requirements.txt

COPY . /csv-service

RUN alembic upgrade head
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]

HEALTHCHECK --interval=20s --timeout=5s \
    CMD curl --fail http://0.0.0.0:8000/api/v1/manage/healthz

EXPOSE 8000
