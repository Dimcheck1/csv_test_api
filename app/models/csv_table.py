from sqlalchemy import Column, Integer, String, DateTime

from app.db.session import Base


class CSVRowTable(Base):
    __tablename__ = "CSVRowTable"

    id = Column(Integer, primary_key=True)
    category = Column(String, index=True)
    firstname = Column(String)
    lastname = Column(String)
    email = Column(String, index=True)
    gender = Column(String, index=True)
    birth_date = Column(String)



