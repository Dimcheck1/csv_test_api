from pydantic import BaseModel


class CSVRowEntity(BaseModel):
    category: str
    firstname: str
    lastname: str
    email: str
    gender: str
    birth_date: str
