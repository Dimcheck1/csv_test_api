import os
from dotenv import load_dotenv
from pydantic_settings import BaseSettings

load_dotenv()


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    PROJECT_NAME: str = "CSV-Service"
    CSV_FIELDS: list = ['category', 'firstname', 'lastname', 'email', 'gender', 'birthDate']

    POSTGRES_USER:       str = os.getenv('POSTGRES_USER')
    POSTGRES_PASSWORD:   str = os.getenv('POSTGRES_PASSWORD')
    POSTGRES_DB:         str = os.getenv('POSTGRES_DB')
    POSTGRES_PORT:       int = os.getenv('POSTGRES_PORT', 5432)
    POSTGRES_SERVER:     str = os.getenv('POSTGRES_SERVER')
    DATABASE_URL:        str = f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}"

    class Config:
        env_file = ".env"


settings = Settings()
