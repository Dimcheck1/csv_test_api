from typing import List
from sqlalchemy.orm import Session

from app.models.csv_table import CSVRowTable


def create_bulk_csv_entry(
    data: List[CSVRowTable],
    db: Session,
) -> CSVRowTable:

    db.bulk_save_objects(data)
    db.commit()
    return data


def create_csv_entry(
    data: List,
) -> CSVRowTable:

    entry = CSVRowTable(
        category=data[0].strip(),
        firstname=data[1],
        lastname=data[2],
        email=data[3],
        gender=data[4],
        birth_date=data[5].strip(),
    )
    return entry


def read_multiple(
    db: Session,
    filter_by: str,
    value_by: str,
    skip: int = 0,
    limit: int = 100
) -> List[CSVRowTable]:

    query = db.query(CSVRowTable)

    if filter_by:
        query = query.filter_by(
            **{filter_by: value_by}
        )

    return query.offset(skip).limit(limit).all()


def read_all(
    db: Session,
    filter_by: str,
    value_by: str,
) -> List[CSVRowTable]:

    query = db.query(CSVRowTable)

    if filter_by:
        query = query.filter_by(
            **{filter_by: value_by}
        )

    return query.all()


def read_csv(file: object, db: Session, step: int = 0) -> None:
    bulk = []
    csv_data = file.file.readlines()[step:]

    for line in csv_data:
        if len(bulk) <= 100:
            bulk.append(create_csv_entry(line.decode('ascii').split(',')))
        else:
            create_bulk_csv_entry(bulk, db)
            bulk.clear()
            step += 100


def delete_all_entries(db: Session) -> int:
    return db.query(CSVRowTable).delete()
