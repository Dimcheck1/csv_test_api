import io
import csv

from sqlalchemy.orm import Session
from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException, UploadFile, File, Query
from fastapi.responses import StreamingResponse

from app.schemas.csv_table import CSVRowEntity
from app.api.deps import get_db
from app.core.config import settings

import app.crud.manage_csv as csv_crud

router = APIRouter()


@router.get("/healthz", status_code=200)
async def liveness_check() -> Any:
    return "Liveness check succeeded."


@router.post("/store_csv")
async def store_csv(
    file: UploadFile = File(...),
    db: Session = Depends(get_db),
) -> Any:

    csv_crud.read_csv(file, db)
    return 'DataSet is being uploaded'


@router.post("/export_csv")
async def export_csv(
    value_by: str | None = None,
    filter_by: str = Query(None, enum=settings.CSV_FIELDS),
    skip: int = 0,
    limit: int = 100,
    db: Session = Depends(get_db),
) -> Any:

    csv_filename = "exported_data.csv"
    entries = csv_crud.read_multiple(db, filter_by, value_by, skip, limit)

    if not entries:
        raise HTTPException(status_code=404, detail="No data found for export.")

    buffer = io.StringIO()
    writer = csv.writer(buffer)
    writer.writerow(settings.CSV_FIELDS)

    for entry in entries:
        writer.writerow([
            getattr(entry, "category", None),
            getattr(entry, "firstname", None),
            getattr(entry, "lastname", None),
            getattr(entry, "email", None),
            getattr(entry, "gender", None),
            getattr(entry, "birth_date", None)
        ])

    buffer.seek(0)
    response = StreamingResponse(
        iter([buffer.getvalue()]),
        media_type="text/csv",
        headers={
            "Content-Disposition": f"attachment; filename={csv_filename}",
            "Content-Type": "application/csv",
        }
    )

    return response


@router.get("/read_csv", response_model=List[CSVRowEntity] | Any)
async def read_csv(
    value_by: str | None = None,
    filter_by: str = Query(None, enum=settings.CSV_FIELDS),
    skip: int = 0,
    limit: int = 100,
    db: Session = Depends(get_db),
) -> Any:

    return csv_crud.read_multiple(db, filter_by, value_by, skip, limit)


@router.delete("/delete_entries")
async def delete_all_csv(
    db: Session = Depends(get_db),
) -> Any:

    return csv_crud.delete_all_entries(db)
