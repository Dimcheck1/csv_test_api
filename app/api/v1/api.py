from fastapi import APIRouter
from app.api.v1.endpoints import csv_convertion


api_router = APIRouter()


api_router.include_router(csv_convertion.router, prefix="/manage", tags=["csv_management"])
