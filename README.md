# csv_test_api

## Important Note
You need to have .env file in project directory

## Local setup
```
python3 -m venv env
pip install -r requirements.txt
docker run --name csv_service -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=1111 -e POSTGRES_DB=csv-service -d postgres:13.3
alembic upgrade head
uvicorn app.main:app --reload
```

## Docker setup
```
docker build -t csv_test .

docker run --name csv_cont -p 80:80 csv_test
```



## How to use

http://0.0.0.0/docs#

You have 3 self-explanatory endpoints

To Fill up the database use Store CSV endpoint(it may take some time)

To Read data from DB, use Read CSV endpoint
 - use skip and limit to manage amount of data in response
 - use filter_by to enable filtering for some column

To Export data from DB, use Export CSV endpoint
 - use filter_by to enable filtering for some column
 - use skip and limit to manage amount of data in response




